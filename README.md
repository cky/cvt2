# CVT v2.0 reduced-blanking timing generator

`cvt2` is a library for generating CVT v2.0 reduced-blanking timings,
in particular RB Timing v2 and RB Timing v3. There are many existing
libraries that support RB Timing v1, but do not support later versions.

For ease of comparison with other libraries, this library also supports
RB Timing v1, but without any of the interlace or margin options.

## Features

+ Supports RB Timing v2 and v3, including all the options supported by
  those versions.
+ Uses only integer arithmetic, allowing for exact results.
    + The unit tests cover various resolutions and refresh rates against
      what's computed by VESA's spreadsheet, to ensure that the results
      are identical.
    + If you find a case that doesn't match, please file an issue! But
      if you're using extra vertical blanking, be aware that the way
      the horizontal period is computed is different between CVT v1.2
      and CVT v2.0; the spreadsheet uses the former and this library
      uses the latter. These don't count as bugs.

## Command-line tool

There is also a `cvt2` example tool that produces output in a similar
format to cvt(1). This allows easy comparison between the two programs.
In particular, cvt(1) uses [libxcvt], which uses floating-point
arithmetic, and produces different results.

(It's an example rather than a main bin because I am eventually going to
add other backends like libxcvt, as a way to further compare results,
and it wouldn't do to add those as dependencies of the core library.)

### Usage

```
Usage: cvt2 <-1|-2|-3> [OPTIONS] <WIDTH> <HEIGHT> [REFRESH_RATE]

Arguments:
  <WIDTH>         The number of active pixels per line
  <HEIGHT>        The number of active lines
  [REFRESH_RATE]  The refresh rate to use [default: 60]

Options:
  -v, --verbose                 Displays cvt(1)-style comment line in
                                addition to the modeline
      --debug                   Displays the modeline in debug format
  -h, --help                    Print help
      --video-opt               Use video-optimised pixel clock [v2 only]
      --early-v-sync            Use early VSync location [v3 only]
      --h-blank-extra <PIXELS>  Additional pixels to add to HBlank [v3 only]
      --v-blank-extra <µs>      Additional microseconds to add to VBlank
                                [v3 only]
```

## Author

[C. K. Young][cky]

[libxcvt]: https://gitlab.freedesktop.org/xorg/lib/libxcvt
[cky]: https://gitlab.com/cky
