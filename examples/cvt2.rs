use clap::{Args, Parser, Subcommand};

use cvt2::{rb_timing_v1, rb_timing_v2, rb_timing_v3, ModeLine};

#[derive(Clone, Debug, Parser)]
/// Computes Coordinated Video Timings.
struct Cli {
    /// Displays cvt(1)-style comment line in addition to the modeline.
    #[arg(short, long, global = true)]
    verbose: bool,

    /// Displays the modeline in debug format.
    #[arg(long, global = true)]
    debug: bool,

    #[command(subcommand)]
    rb_timing: RbTiming,
}

#[derive(Args, Clone, Debug)]
struct Common {
    /// The number of active pixels per line.
    width: u32,

    /// The number of active lines.
    height: u32,

    /// The refresh rate to use.
    #[arg(default_value_t = 60)]
    refresh_rate: u32,
}

#[derive(Clone, Debug, Subcommand)]
enum RbTiming {
    /// Use RB Timing v1.
    #[command(short_flag = '1')]
    V1 {
        #[command(flatten)]
        common: Common,
    },

    /// Use RB Timing v2.
    #[command(short_flag = '2')]
    V2 {
        #[command(flatten)]
        common: Common,

        /// Use video-optimised pixel clock.
        #[arg(long)]
        video_opt: bool,
    },

    /// Use RB Timing v3.
    #[command(short_flag = '3')]
    V3 {
        #[command(flatten)]
        common: Common,

        /// Use early VSync location.
        #[arg(long, alias = "early-vsync")]
        early_v_sync: bool,

        /// Additional pixels to add to HBlank.
        #[arg(long, default_value_t, alias = "hblank-extra", value_name = "PIXELS")]
        h_blank_extra: u32,

        /// Additional microseconds to add to VBlank.
        #[arg(long, default_value_t, alias = "vblank-extra", value_name = "µs")]
        v_blank_extra: u32,
    },
}

impl RbTiming {
    fn calculate(&self) -> ModeLine {
        match *self {
            Self::V1 {
                common:
                    Common {
                        width,
                        height,
                        refresh_rate,
                    },
            } => rb_timing_v1(width, height, refresh_rate),
            Self::V2 {
                common:
                    Common {
                        width,
                        height,
                        refresh_rate,
                    },
                video_opt,
            } => rb_timing_v2(width, height, refresh_rate, video_opt),
            Self::V3 {
                common:
                    Common {
                        width,
                        height,
                        refresh_rate,
                    },
                early_v_sync,
                h_blank_extra,
                v_blank_extra,
            } => rb_timing_v3(width, height, refresh_rate, early_v_sync, h_blank_extra, v_blank_extra),
        }
    }
}

fn main() {
    let cli = Cli::parse();
    let mode_line = cli.rb_timing.calculate();
    match (cli.verbose, cli.debug) {
        (false, false) => println!("{mode_line}"),
        (false, true) => println!("{mode_line:?}"),
        (true, false) => println!("{mode_line:#}"),
        (true, true) => println!("{mode_line:#?}"),
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn debug_assert() {
        use clap::CommandFactory;
        super::Cli::command().debug_assert()
    }
}
