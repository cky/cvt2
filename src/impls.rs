use std::fmt;

use crate::types::{ModeLine, Polarity};

const H_SYNC: u32 = 32;
const MIN_V_BLANK: u32 = 460;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum AspectRatio {
    Standard(u8, u8),
    Custom,
}

impl fmt::UpperHex for AspectRatio {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::Standard(_, y) => y.fmt(f),
            Self::Custom => Ok(()),
        }
    }
}

trait RbTiming {
    const CELL_GRAN: u32;
    const FIELD_RATE_FACTOR: u64;
    const FIELD_RATE_ADJ: u64 = 0;
    const MIN_H_BLANK: u32;
    const H_FRONT_PORCH: u32;
    const MIN_V_FRONT_PORCH: u32;
    const MIN_V_BACK_PORCH: u32;

    fn round_width(width: u32) -> u32 {
        width & !(Self::CELL_GRAN - 1)
    }

    fn aspect_ratio(&self) -> AspectRatio {
        let (width, height) = (self.h_active(), self.v_active());
        if width == Self::round_width(height * 4 / 3) {
            AspectRatio::Standard(4, 3)
        } else if width == Self::round_width(height * 16 / 9) || (width, height) == (1366, 768) {
            AspectRatio::Standard(16, 9)
        } else if width == Self::round_width(height * 8 / 5) {
            AspectRatio::Standard(16, 10)
        } else if width == Self::round_width(height * 5 / 4) {
            AspectRatio::Standard(5, 4)
        } else if width == Self::round_width(height * 15 / 9) {
            AspectRatio::Standard(15, 9)
        } else {
            AspectRatio::Custom
        }
    }

    fn name(&self) -> String {
        format!(
            "{:.2}M{:X}-R",
            f64::from(self.h_active() * self.v_active()) / 1_000_000.0,
            self.aspect_ratio()
        )
    }

    fn field_rate(&self) -> u64 {
        u64::from(self.refresh_rate()) * (Self::FIELD_RATE_FACTOR + Self::FIELD_RATE_ADJ)
    }

    fn refresh_rate(&self) -> u32;
    fn v_blank_time(&self) -> u32;
    fn min_vbi(&self) -> u32 {
        Self::MIN_V_FRONT_PORCH + self.v_sync() + Self::MIN_V_BACK_PORCH
    }
    fn vbi_lines(&self) -> u32 {
        let field_rate = self.field_rate();
        (field_rate * u64::from(self.v_blank_time() * self.v_active()))
            .div_ceil(Self::FIELD_RATE_FACTOR * 1_000_000 - field_rate * u64::from(MIN_V_BLANK))
            .try_into()
            .unwrap()
    }

    fn h_active(&self) -> u32;
    fn h_blank(&self) -> u32;
    fn h_back_porch(&self) -> u32 {
        self.h_blank() - Self::H_FRONT_PORCH - H_SYNC
    }
    fn h_total(&self) -> u32 {
        self.h_active() + self.h_blank()
    }

    fn v_active(&self) -> u32;
    fn v_blank(&self) -> u32 {
        self.min_vbi().max(self.vbi_lines())
    }
    fn v_front_porch(&self) -> u32 {
        self.v_blank() - self.v_back_porch() - self.v_sync()
    }
    fn v_sync(&self) -> u32;
    fn v_back_porch(&self) -> u32 {
        self.v_blank() - self.v_front_porch() - self.v_sync()
    }
    fn v_total(&self) -> u32 {
        self.v_active() + self.v_blank()
    }

    fn raw_frequency(&self) -> u64 {
        self.field_rate() * u64::from(self.v_total() * self.h_total())
    }
    fn round_pixel_clock(&self, frequency: u64) -> u64;
    fn pixel_clock(&self) -> u64 {
        self.round_pixel_clock(self.raw_frequency())
    }

    fn to_mode_line(&self) -> ModeLine {
        ModeLine {
            name: self.name(),
            pixel_clock: self.pixel_clock(),
            h_disp: self.h_active(),
            h_front_porch: Self::H_FRONT_PORCH,
            h_sync: H_SYNC,
            h_blank: self.h_blank(),
            h_sync_polarity: Polarity::Positive,
            v_disp: self.v_active(),
            v_front_porch: self.v_front_porch(),
            v_sync: self.v_sync(),
            v_blank: self.v_blank(),
            v_sync_polarity: Polarity::Negative,
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct RbTimingV1 {
    width: u32,
    height: u32,
    refresh_rate: u32,
}

impl RbTiming for RbTimingV1 {
    const CELL_GRAN: u32 = 8;
    const FIELD_RATE_FACTOR: u64 = 1;
    const MIN_H_BLANK: u32 = 160;
    const H_FRONT_PORCH: u32 = 48;
    const MIN_V_FRONT_PORCH: u32 = 3;
    const MIN_V_BACK_PORCH: u32 = 7;

    fn refresh_rate(&self) -> u32 {
        self.refresh_rate
    }

    fn v_blank_time(&self) -> u32 {
        MIN_V_BLANK
    }

    fn h_active(&self) -> u32 {
        Self::round_width(self.width)
    }

    fn v_active(&self) -> u32 {
        self.height
    }

    fn h_blank(&self) -> u32 {
        Self::MIN_H_BLANK
    }

    fn v_front_porch(&self) -> u32 {
        Self::MIN_V_FRONT_PORCH
    }

    fn v_sync(&self) -> u32 {
        match self.aspect_ratio() {
            AspectRatio::Standard(4, 3) => 4,
            AspectRatio::Standard(16, 9) => 5,
            AspectRatio::Standard(16, 10) => 6,
            AspectRatio::Standard(5, 4) | AspectRatio::Standard(15, 9) => 7,
            _ => 10,
        }
    }

    fn round_pixel_clock(&self, frequency: u64) -> u64 {
        frequency - frequency % 250_000
    }
}

#[derive(Clone, Copy, Debug)]
struct RbTimingV2 {
    width: u32,
    height: u32,
    refresh_rate: u32,
    video_opt: bool,
}

impl RbTiming for RbTimingV2 {
    const CELL_GRAN: u32 = 1;
    const FIELD_RATE_FACTOR: u64 = 1;
    const MIN_H_BLANK: u32 = 80;
    const H_FRONT_PORCH: u32 = 8;
    const MIN_V_FRONT_PORCH: u32 = 1;
    const MIN_V_BACK_PORCH: u32 = 6;

    fn refresh_rate(&self) -> u32 {
        self.refresh_rate
    }

    fn v_blank_time(&self) -> u32 {
        MIN_V_BLANK
    }

    fn h_active(&self) -> u32 {
        self.width
    }

    fn v_active(&self) -> u32 {
        self.height
    }

    fn h_blank(&self) -> u32 {
        Self::MIN_H_BLANK
    }

    fn v_sync(&self) -> u32 {
        8
    }

    fn v_back_porch(&self) -> u32 {
        Self::MIN_V_BACK_PORCH
    }

    fn round_pixel_clock(&self, frequency: u64) -> u64 {
        if self.video_opt {
            frequency / 1001 * 1000
        } else {
            frequency - frequency % 1000
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct RbTimingV3 {
    width: u32,
    height: u32,
    refresh_rate: u32,
    early_v_sync: bool,
    h_blank_extra: u32,
    v_blank_extra: u32,
}

impl RbTiming for RbTimingV3 {
    const CELL_GRAN: u32 = 8;
    const FIELD_RATE_FACTOR: u64 = 20000;
    const FIELD_RATE_ADJ: u64 = 7;
    const MIN_H_BLANK: u32 = 80;
    const H_FRONT_PORCH: u32 = 8;
    const MIN_V_FRONT_PORCH: u32 = 1;
    const MIN_V_BACK_PORCH: u32 = 6;

    fn refresh_rate(&self) -> u32 {
        self.refresh_rate
    }

    fn v_blank_time(&self) -> u32 {
        MIN_V_BLANK + self.v_blank_extra
    }

    fn h_active(&self) -> u32 {
        Self::round_width(self.width)
    }

    fn v_active(&self) -> u32 {
        self.height
    }

    fn h_blank(&self) -> u32 {
        Self::MIN_H_BLANK + Self::round_width(self.h_blank_extra)
    }

    fn v_sync(&self) -> u32 {
        8
    }

    fn v_back_porch(&self) -> u32 {
        if self.early_v_sync {
            self.vbi_lines() / 2
        } else {
            Self::MIN_V_BACK_PORCH
        }
    }

    fn round_pixel_clock(&self, frequency: u64) -> u64 {
        frequency.div_ceil(Self::FIELD_RATE_FACTOR * 1000) * 1000
    }
}

#[must_use]
pub fn rb_timing_v1(width: u32, height: u32, refresh_rate: u32) -> ModeLine {
    RbTimingV1 {
        width,
        height,
        refresh_rate,
    }
    .to_mode_line()
}

#[must_use]
pub fn rb_timing_v2(width: u32, height: u32, refresh_rate: u32, video_opt: bool) -> ModeLine {
    RbTimingV2 {
        width,
        height,
        refresh_rate,
        video_opt,
    }
    .to_mode_line()
}

#[must_use]
pub fn rb_timing_v3(
    width: u32,
    height: u32,
    refresh_rate: u32,
    early_v_sync: bool,
    h_blank_extra: u32,
    v_blank_extra: u32,
) -> ModeLine {
    RbTimingV3 {
        width,
        height,
        refresh_rate,
        early_v_sync,
        h_blank_extra,
        v_blank_extra,
    }
    .to_mode_line()
}
