mod impls;
mod types;

pub use impls::{rb_timing_v1, rb_timing_v2, rb_timing_v3};
pub use types::ModeLine;

// The numbers for these tests come from the official VESA CVT v2.0 Generator
// spreadsheet. The numbers produced by these functions are required to match
// the spreadsheet exactly, except I had to fix one place where they used the
// CVT v1.2 E2 formula (instead of CVT v2.0) for computing `H_PERIOD_EST`.
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_3840_2160_60() {
        assert_eq!(
            rb_timing_v1(3840, 2160, 60).to_string(),
            "ModeLine \"8.29M9-R\"  533.250  3840 3888 3920 4000  2160 2163 2168 2222  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(3840, 2160, 60, false).to_string(),
            "ModeLine \"8.29M9-R\"  522.614  3840 3848 3880 3920  2160 2208 2216 2222  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(3840, 2160, 60, true).to_string(),
            "ModeLine \"8.29M9-R\"  522.092  3840 3848 3880 3920  2160 2208 2216 2222  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3840, 2160, 60, false, 0, 0).to_string(),
            "ModeLine \"8.29M9-R\"  522.798  3840 3848 3880 3920  2160 2208 2216 2222  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3840, 2160, 60, true, 0, 0).to_string(),
            "ModeLine \"8.29M9-R\"  522.798  3840 3848 3880 3920  2160 2183 2191 2222  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3840, 2160, 60, false, 120, 245).to_string(),
            "ModeLine \"8.29M9-R\"  546.561  3840 3848 3880 4040  2160 2240 2248 2254  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3840, 2160, 60, true, 120, 245).to_string(),
            "ModeLine \"8.29M9-R\"  546.561  3840 3848 3880 4040  2160 2199 2207 2254  +HSync -VSync",
        );
    }

    #[test]
    fn test_3456_2160_120() {
        assert_eq!(
            rb_timing_v1(3456, 2160, 120).to_string(),
            "ModeLine \"7.46MA-R\"  992.250  3456 3504 3536 3616  2160 2163 2169 2287  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(3456, 2160, 120, false).to_string(),
            "ModeLine \"7.46MA-R\"  970.419  3456 3464 3496 3536  2160 2273 2281 2287  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(3456, 2160, 120, true).to_string(),
            "ModeLine \"7.46MA-R\"  969.450  3456 3464 3496 3536  2160 2273 2281 2287  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3456, 2160, 120, false, 0, 0).to_string(),
            "ModeLine \"7.46MA-R\"  970.760  3456 3464 3496 3536  2160 2273 2281 2287  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3456, 2160, 120, true, 0, 0).to_string(),
            "ModeLine \"7.46MA-R\"  970.760  3456 3464 3496 3536  2160 2216 2224 2287  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3456, 2160, 120, false, 120, 245).to_string(),
            "ModeLine \"7.46MA-R\"  1033.109  3456 3464 3496 3656  2160 2340 2348 2354  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(3456, 2160, 120, true, 120, 245).to_string(),
            "ModeLine \"7.46MA-R\"  1033.109  3456 3464 3496 3656  2160 2249 2257 2354  +HSync -VSync",
        );
    }

    #[test]
    fn test_2560_1600_240() {
        assert_eq!(
            rb_timing_v1(2560, 1600, 240).to_string(),
            "ModeLine \"4.10MA-R\"  1174.250  2560 2608 2640 2720  1600 1603 1609 1799  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(2560, 1600, 240, false).to_string(),
            "ModeLine \"4.10MA-R\"  1139.846  2560 2568 2600 2640  1600 1785 1793 1799  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(2560, 1600, 240, true).to_string(),
            "ModeLine \"4.10MA-R\"  1138.707  2560 2568 2600 2640  1600 1785 1793 1799  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(2560, 1600, 240, false, 0, 0).to_string(),
            "ModeLine \"4.10MA-R\"  1140.246  2560 2568 2600 2640  1600 1785 1793 1799  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(2560, 1600, 240, true, 0, 0).to_string(),
            "ModeLine \"4.10MA-R\"  1140.246  2560 2568 2600 2640  1600 1692 1700 1799  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(2560, 1600, 240, false, 120, 245).to_string(),
            "ModeLine \"4.10MA-R\"  1262.314  2560 2568 2600 2760  1600 1891 1899 1905  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(2560, 1600, 240, true, 120, 245).to_string(),
            "ModeLine \"4.10MA-R\"  1262.314  2560 2568 2600 2760  1600 1745 1753 1905  +HSync -VSync",
        );
    }

    #[test]
    fn test_1920_1080_360() {
        assert_eq!(
            rb_timing_v1(1920, 1080, 360).to_string(),
            "ModeLine \"2.07M9-R\"  969.500  1920 1968 2000 2080  1080 1083 1088 1295  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(1920, 1080, 360, false).to_string(),
            "ModeLine \"2.07M9-R\"  932.400  1920 1928 1960 2000  1080 1281 1289 1295  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v2(1920, 1080, 360, true).to_string(),
            "ModeLine \"2.07M9-R\"  931.468  1920 1928 1960 2000  1080 1281 1289 1295  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(1920, 1080, 360, false, 0, 0).to_string(),
            "ModeLine \"2.07M9-R\"  932.727  1920 1928 1960 2000  1080 1281 1289 1295  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(1920, 1080, 360, true, 0, 0).to_string(),
            "ModeLine \"2.07M9-R\"  932.727  1920 1928 1960 2000  1080 1180 1188 1295  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(1920, 1080, 360, false, 120, 245).to_string(),
            "ModeLine \"2.07M9-R\"  1075.726  1920 1928 1960 2120  1080 1395 1403 1409  +HSync -VSync",
        );
        assert_eq!(
            rb_timing_v3(1920, 1080, 360, true, 120, 245).to_string(),
            "ModeLine \"2.07M9-R\"  1075.726  1920 1928 1960 2120  1080 1237 1245 1409  +HSync -VSync",
        );
    }
}
