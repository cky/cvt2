use std::fmt;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ModeLine {
    pub name: String,
    pub pixel_clock: u64,
    pub h_disp: u32,
    pub h_front_porch: u32,
    pub h_sync: u32,
    pub h_blank: u32,
    pub h_sync_polarity: Polarity,
    pub v_disp: u32,
    pub v_front_porch: u32,
    pub v_sync: u32,
    pub v_blank: u32,
    pub v_sync_polarity: Polarity,
}

impl ModeLine {
    fn h_sync_start(&self) -> u32 {
        self.h_disp + self.h_front_porch
    }

    fn h_sync_end(&self) -> u32 {
        self.h_sync_start() + self.h_sync
    }

    fn h_total(&self) -> u32 {
        self.h_disp + self.h_blank
    }

    fn v_sync_start(&self) -> u32 {
        self.v_disp + self.v_front_porch
    }

    fn v_sync_end(&self) -> u32 {
        self.v_sync_start() + self.v_sync
    }

    fn v_total(&self) -> u32 {
        self.v_disp + self.v_blank
    }
}

impl fmt::Display for ModeLine {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pixel_clock = self.pixel_clock as f64;
        if f.alternate() {
            writeln!(
                f,
                "# {}x{} {:.3} Hz; hsync: {:.3} kHz; pclk: {:.3} MHz",
                self.h_disp,
                self.v_disp,
                pixel_clock / f64::from(self.h_total()) / f64::from(self.v_total()),
                pixel_clock / f64::from(self.h_total()) / 1000.0,
                pixel_clock / 1_000_000.0,
            )?;
        }
        write!(
            f,
            "ModeLine {:?}  {:.3}  {} {} {} {}  {} {} {} {}  {}HSync {}VSync",
            self.name,
            pixel_clock / 1_000_000.0,
            self.h_disp,
            self.h_sync_start(),
            self.h_sync_end(),
            self.h_total(),
            self.v_disp,
            self.v_sync_start(),
            self.v_sync_end(),
            self.v_total(),
            self.h_sync_polarity,
            self.v_sync_polarity,
        )
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Polarity {
    Positive,
    Negative,
}

impl fmt::Display for Polarity {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::Positive => '+',
            Self::Negative => '-',
        }
        .fmt(f)
    }
}
